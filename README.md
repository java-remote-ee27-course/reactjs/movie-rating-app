# Movie rating app

## Description

A movie rating app to search and rate movies from the web.

- See initial video of the project: https://youtu.be/n1VzyUvnNq0

## Functionalities

- you may search and fetch movies from the web (the app uses OMDb API)
- implemented the error handling
- give star-rating to a movie
- view movie details
- see spinner when movies or movie detail view is loading
- add movies to user's watched movies' list, including user rating given to the movie
- see summary statistics based by user's watched list data (user rating, imdb rating, time averages)
- keep the state of movie StarRating component when reloading the MovieDetailView
- update userRating when user gives a different rating to the movie
- remove movie(s) from the watched list (it also resets the user rating)
- summary averages are calculated based on user's movie list data
- separate re-usable star-rating component
- dynamical title rendering (tab title changes when you select a movie) (useEffect)
- cleanup functions for App component data fetching useEffect and to the MovieDetailView title rendering useEffect
- the listener for Esc key events (in MovieDetailView component, to exit with keypress) (useEffect)
- after displaying the search results, previous movie detail view is not displayed anymore
- save watched movies data to local storage, and fetch it later (useEffect)
- hitting "Enter" outside the Search-box focuses Search and removes old query results from the screen (useRef)
- **[NEW]** added custom re-usable hooks: useKeys, useLocalStorage, useMovies.
- **[NEW]** useKey is re-used for Escape (MovieDetailView.js) and Enter (Search.js) keys
- refactored the code

## To-do

- additional refactor of code
- consider changing the useEffect (data fetching in App.js) to event handler function, as the data is no more fetched on mount while loading the app.

## Run the code

To run it, you would need Node.js installed in your computer and OMDb API key

- Get an API key from https://www.omdbapi.com/

- Create an .env file in the project root folder

- in the .env file add this row:

  `REACT_APP_OMDB_API_KEY = <Place your API key here>`

- install the latest node.js

- `git clone https://gitlab.com/java-remote-ee27-course/reactjs/movie-rating-app.git`

- go inside the downloaded project folder and install dependencies for ReactJS: `npm i`

- open the project folder in VS Code (or in some other editor)

- open terminal and `run npm start`

- navigate to page: http://localhost:3000

## Created by

Katlin - most of the JSX part (some initial static components (without interactivity) I took from the https://www.udemy.com/course/the-ultimate-react-course/learn/lecture/37350716) and refactored the code to separate components (it was one big component at first)

Styles based on https://www.udemy.com/course/the-ultimate-react-course/learn/lecture/37350716

## Screenshots

Search movies from OMDb:
![movies4](./public/assets/movies4.png)

View movie details, rate movies

![movies5](./public/assets/movies5.png)

Add to watched list button is shown only if movie is rated and it is not previously added to list already:

![movies6](./public/assets/movies6.png)

- After adding a movie to the watched list, the list is displayed to the user.
- You may see the summary statistics of watched movies, see both IMDb and your ratings etc.

![movies8](./public/assets/movies8.png)

Update a watched movie rating

![movies10](./public/assets/movies10.png)

Delete a movie from the list (it resets the user rating as well)

![movies9](./public/assets/movies9.png)

Save watched movies data and fetch them from the local storage

![movies11](./public/assets/movies11.png)

import { useEffect, useState } from "react";
import { MovieDetailView } from "./MovieDetailView";
import { WatchedMovieList } from "./WatchedMovieList";
import { Summary } from "./Summary";
import { MovieList } from "./MovieList";
import { Box } from "./Box";
import { NavBar } from "./NavBar";
import { Logo } from "./Logo";
import { Search } from "./Search";
import { ResultsNumber } from "./ResultsNumber";
import { Main } from "./Main";
import { Loader } from "./Loader";
import { ErrorMessage } from "./ErrorMessage";

export const average = (arr) =>
  arr.reduce((acc, cur, i, arr) => acc + cur / arr.length, 0);

export const KEY = process.env.REACT_APP_OMDB_API_KEY;

export default function App() {
  const [query, setQuery] = useState("");
  const [error, setError] = useState("");
  const [movies, setMovies] = useState([]);
  const [watched, setWatched] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [selectedId, setSelectedId] = useState(null);

  //to open/close movie details view
  function handleSelectMovie(id) {
    setSelectedId((selectedId) => (id === selectedId ? null : id));
  }

  //close movie details view
  function handleCloseMovie() {
    setSelectedId(null);
  }

  function handleAddWatched(movie) {
    setWatched((watched) => [...watched, movie]);
  }

  function handleDeleteWatched(id) {
    setWatched((watched) =>
      watched.filter((watchedMovie) => watchedMovie.imdbID !== id)
    );
  }

  useEffect(() => {
    //browser API (for later cleanup) and connect AbortController with signal
    const controller = new AbortController();

    //setting timeout in order not to perform queries for each letter
    //change the timeout time, depending how long you want to wait
    const waitFetch = setTimeout(async function fetchMovies() {
      try {
        setError("");
        setIsLoading(true);
        if (query === "") return;

        const response = await fetch(
          `http://www.omdbapi.com/?apikey=${KEY}&s=${query}`,
          { signal: controller.signal }
        );

        const data = await response.json();

        if (data.Response === "False") {
          throw new Error("No movies found containing: " + query);
        }

        handleCloseMovie();
        setMovies(data.Search);
        setError("");
      } catch (err) {
        if (err.name !== "AbortError") {
          setError(err.message);
        }
      } finally {
        setIsLoading(false);
      }
    }, 500);

    //cleanup function:
    return function () {
      controller.abort();
      clearTimeout(waitFetch);
    };
  }, [query]);

  return (
    <>
      <NavBar>
        <Logo />
        <Search query={query} setQuery={setQuery} />
        <ResultsNumber movies={movies} />
      </NavBar>
      <Main>
        <Box>
          {isLoading && <Loader />}
          {!isLoading && !error && (
            <MovieList movies={movies} onSelectMovie={handleSelectMovie} />
          )}
          {error && <ErrorMessage message={error} />}
        </Box>
        <Box>
          {selectedId === null ? (
            <>
              <Summary watched={watched} />
              <WatchedMovieList
                watched={watched}
                onDeleteWatched={handleDeleteWatched}
              />
            </>
          ) : (
            <>
              <MovieDetailView
                selectedId={selectedId}
                onCloseMovie={handleCloseMovie}
                watched={watched}
                onAddWatched={handleAddWatched}
                onDeleteWatched={handleDeleteWatched}
              />
            </>
          )}
        </Box>
      </Main>
    </>
  );
}

import { useEffect, useState } from "react";
import StarRating from "./StarRating";
import { KEY } from "./App";
import { ErrorMessage } from "./ErrorMessage";
import { Loader } from "./Loader";

export function MovieDetailView({
  selectedId,
  onCloseMovie,
  watched,
  onAddWatched,
  onDeleteWatched,
}) {
  const [selectedMovie, setSelectedMovie] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");
  const [userRating, setUserRating] = useState("");

  const {
    Title: title,
    Poster: poster,
    imdbRating,
    Released: released,
    Runtime: runtime,
    Director: director,
    Actors: actors,
    Plot: plot,
    Genre: genre,
  } = selectedMovie;

  const isWatched = watched.map((movie) => movie.imdbID).includes(selectedId);

  //  See if movie is already in the list and get its user rating:
  const watchedRating = watched.find(
    (watchedMovie) => watchedMovie.imdbID === selectedMovie.imdbID
  )?.userRating;

  //I.e. if runtime is "123 min", split it to get the number part of it
  const runtimeToNumber = Number(runtime?.split(" ").at(0));

  //create new movie object to add it to list (or to update)
  const watchedMovie = {
    imdbID: selectedId,
    title,
    poster,
    userRating,
    imdbRating: Number(imdbRating),
    runtime: runtimeToNumber,
  };

  function handleAddWatched() {
    //if found, do nothing
    if (isWatched) return;

    //if movie is not in the list, then add it, and close detail view (show the list instead)
    onAddWatched(watchedMovie);
    onCloseMovie();
  }
  function handleUpdateRating() {
    if (!isWatched) return;

    onDeleteWatched(watchedMovie.imdbID);
    onAddWatched(watchedMovie);
    onCloseMovie();
  }

  //Here no cleanup, as movies are clicked one-by-one, expected to load them all
  useEffect(
    function () {
      async function fetchMovie() {
        try {
          setIsLoading(true);
          const response = await fetch(
            `http://www.omdbapi.com/?apikey=${KEY}&i=${selectedId}`
          );
          const data = await response.json();
          setSelectedMovie(data);
        } catch (err) {
          setError(err.message);
        } finally {
          setIsLoading(false);
        }
      }
      fetchMovie();
    },
    [selectedId]
  );

  //change the tab title when user opens movie details
  useEffect(() => {
    if (!title) return;
    document.title = title;

    //cleanup function (reverts the title back when effect has ended):
    return function () {
      document.title = "Movie App";
    };
  }, [title]);

  // ESC key closes movie detail view.
  // Each time effect is executed, one more eventlistener is added,
  // so cleanup is needed (otherwise might produce memory problems in large apps)
  useEffect(() => {
    function callback(e) {
      if (e.code === "Escape") {
        onCloseMovie();
      }
    }

    document.addEventListener("keydown", callback);

    return function () {
      document.removeEventListener("keydown", callback);
    };
  }, [onCloseMovie]);

  return (
    <div className={isLoading ? "details loader-container" : "details"}>
      {isLoading && <Loader />}
      {error && <ErrorMessage message={error} />}
      {!error && !isLoading && (
        <>
          <header>
            <button className="btn-back" onClick={onCloseMovie}>
              &larr;
            </button>

            <img src={poster} alt={`${title} poster`} />
            <div className="details-overview">
              <h2>{title}</h2>
              <p>
                {released} - {runtime}
              </p>

              <p>{genre}</p>
              <p>
                <span>⭐</span>
                {imdbRating} IMDb rating
              </p>
            </div>
          </header>

          <section>
            <div className="rating">
              <StarRating
                maxRating={10}
                size="32"
                defaultRating={isWatched ? watchedRating : 0}
                onSetMovieRating={setUserRating}
              />
              {!isWatched && userRating > 0 && (
                <button className="btn-add" onClick={handleAddWatched}>
                  Add to watched list
                </button>
              )}
              {isWatched && userRating > 0 && (
                <button className="btn-add" onClick={handleUpdateRating}>
                  Update rating
                </button>
              )}
            </div>

            <div className="details-overview">
              <h2>{title}</h2>
              <p>{plot}</p>
              <p>Actors: {actors}</p>
              <p>Directed by: {director}</p>
            </div>
          </section>
        </>
      )}
    </div>
  );
}

import { useRef } from "react";
import { useKey } from "./useKey";

export function Search({ query, setQuery }) {
  //Focus search box (when hitting Enter)
  const inputElement = useRef(null);

  useKey("Enter", () => {
    if (document.activeElement === inputElement.current) return;
    inputElement.current.focus();
    setQuery("");
  });

  return (
    <input
      className="search"
      type="text"
      placeholder="Search movies..."
      value={query}
      onChange={(e) => setQuery(e.target.value)}
      ref={inputElement}
      autoFocus
    />
  );
}

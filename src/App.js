import { useState } from "react";
import { MovieDetailView } from "./MovieDetailView";
import { WatchedMovieList } from "./WatchedMovieList";
import { Summary } from "./Summary";
import { MovieList } from "./MovieList";
import { Box } from "./Box";
import { NavBar } from "./NavBar";
import { Logo } from "./Logo";
import { Search } from "./Search";
import { ResultsNumber } from "./ResultsNumber";
import { Main } from "./Main";
import { Loader } from "./Loader";
import { ErrorMessage } from "./ErrorMessage";

import { useLocalStorageState } from "./useLocalStorageState";
import { useMovies } from "./useMovies";

export const average = (arr) =>
  arr.reduce((acc, cur, i, arr) => acc + cur / arr.length, 0);

export const KEY = process.env.REACT_APP_OMDB_API_KEY;

export default function App() {
  const [query, setQuery] = useState("");
  const [selectedId, setSelectedId] = useState(null);
  const { movies, isLoading, error } = useMovies(query /*, handleCloseMovie*/);

  const [watched, setWatched] = useLocalStorageState([], "watched");
  //to open/close movie details view
  function handleSelectMovie(id) {
    setSelectedId((selectedId) => (id === selectedId ? null : id));
  }

  //close movie details view
  function handleCloseMovie() {
    setSelectedId(null);
  }

  function handleAddWatched(movie) {
    setWatched((watched) => [...watched, movie]);
    //alteranative: localStorage.setItem("watched", JSON.stringify([...watched, movie]));
  }

  function handleDeleteWatched(id) {
    setWatched((watched) =>
      watched.filter((watchedMovie) => watchedMovie.imdbID !== id)
    );
  }

  return (
    <>
      <NavBar>
        <Logo />
        <Search query={query} setQuery={setQuery} />
        <ResultsNumber movies={movies} />
      </NavBar>
      <Main>
        <Box>
          {isLoading && <Loader />}
          {!isLoading && !error && (
            <MovieList movies={movies} onSelectMovie={handleSelectMovie} />
          )}
          {error && <ErrorMessage message={error} />}
        </Box>
        <Box>
          {selectedId === null ? (
            <>
              <Summary watched={watched} />
              <WatchedMovieList
                watched={watched}
                onDeleteWatched={handleDeleteWatched}
              />
            </>
          ) : (
            <>
              <MovieDetailView
                selectedId={selectedId}
                onCloseMovie={handleCloseMovie}
                watched={watched}
                onAddWatched={handleAddWatched}
                onDeleteWatched={handleDeleteWatched}
              />
            </>
          )}
        </Box>
      </Main>
    </>
  );
}

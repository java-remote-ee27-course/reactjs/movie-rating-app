import { useEffect, useRef, useState } from "react";
import StarRating from "./StarRating";
import { KEY } from "./App";
import { ErrorMessage } from "./ErrorMessage";
import { Loader } from "./Loader";
import { useKey } from "./useKey";

export function MovieDetailView({
  selectedId,
  onCloseMovie,
  watched,
  onAddWatched,
  onDeleteWatched,
}) {
  const [selectedMovie, setSelectedMovie] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");
  const [userRating, setUserRating] = useState("");

  const countRef = useRef(0);

  useEffect(() => {
    if (userRating) countRef.current++;
  }, [userRating]);

  const {
    Title: title,
    Poster: poster,
    imdbRating,
    Released: released,
    Runtime: runtime,
    Director: director,
    Actors: actors,
    Plot: plot,
    Genre: genre,
  } = selectedMovie;

  const isWatched = watched.map((movie) => movie.imdbID).includes(selectedId);

  //  See if movie is already in the list and get its user rating:
  const watchedRating = watched.find(
    (watchedMovie) => watchedMovie.imdbID === selectedMovie.imdbID
  )?.userRating;

  //I.e. if runtime is "123 min", split it to get the number part of it
  const runtimeToNumber = Number(runtime?.split(" ").at(0));

  function handleAddWatched() {
    //create new movie object to add it to list
    const watchedMovie = {
      imdbID: selectedId,
      title,
      poster,
      userRating,
      imdbRating: Number(imdbRating),
      runtime: runtimeToNumber,
      countRatingDecisions: countRef.current,
    };
    //if found, do nothing
    if (isWatched) return;

    //if movie is not in the list, then add it, and close detail view (show the list instead)
    onAddWatched(watchedMovie);
    onCloseMovie();
  }

  function handleUpdateRating() {
    //create new movie object to update it
    const watchedMovie = {
      imdbID: selectedId,
      title,
      poster,
      userRating,
      imdbRating: Number(imdbRating),
      runtime: runtimeToNumber,
      countRatingDecisions: countRef.current,
    };

    if (!isWatched) return;

    onDeleteWatched(watchedMovie.imdbID);
    onAddWatched(watchedMovie);
    onCloseMovie();
  }

  useKey("Escape", onCloseMovie);
  //Here no cleanup, as movies are clicked one-by-one, expected to load them all
  useEffect(
    function () {
      async function fetchMovie() {
        try {
          setIsLoading(true);
          const response = await fetch(
            `http://www.omdbapi.com/?apikey=${KEY}&i=${selectedId}`
          );
          const data = await response.json();
          setSelectedMovie(data);
        } catch (err) {
          setError(err.message);
        } finally {
          setIsLoading(false);
        }
      }
      fetchMovie();
    },
    [selectedId]
  );

  //change the tab title when user opens movie details
  useEffect(() => {
    if (!title) return;
    document.title = title;

    //cleanup function (reverts the title back when effect has ended):
    return function () {
      document.title = "Movie App";
    };
  }, [title]);

  return (
    <div className={isLoading ? "details loader-container" : "details"}>
      {isLoading && <Loader />}
      {error && <ErrorMessage message={error} />}
      {!error && !isLoading && (
        <>
          <header>
            <button className="btn-back" onClick={onCloseMovie}>
              &larr;
            </button>

            <img src={poster} alt={`${title} poster`} />
            <div className="details-overview">
              <h2>{title}</h2>
              <p>
                {released} - {runtime}
              </p>

              <p>{genre}</p>
              <p>
                <span>⭐</span>
                {imdbRating} IMDb rating
              </p>
            </div>
          </header>
          <section>
            <div className="rating">
              <StarRating
                maxRating={10}
                size="32"
                defaultRating={isWatched ? watchedRating : 0}
                onSetMovieRating={setUserRating}
              />
              {!isWatched && userRating > 0 && (
                <button className="btn-add" onClick={handleAddWatched}>
                  Add to watched list
                </button>
              )}
              {isWatched && userRating > 0 && (
                <button className="btn-add" onClick={handleUpdateRating}>
                  Update rating
                </button>
              )}
            </div>

            <div className="details-overview">
              <h2>{title}</h2>
              <p>{plot}</p>
              <p>Actors: {actors}</p>
              <p>Directed by: {director}</p>
            </div>
          </section>
        </>
      )}
    </div>
  );
}

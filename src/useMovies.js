import { useEffect, useState } from "react";

export const KEY = process.env.REACT_APP_OMDB_API_KEY;

export function useMovies(query /*, callback */) {
  const [error, setError] = useState("");
  const [movies, setMovies] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    //callback?.();
    //browser API (for later cleanup) and connect AbortController with signal
    const controller = new AbortController();

    //setting timeout in order not to perform queries for each letter
    const waitFetch = setTimeout(async function fetchMovies() {
      try {
        setError("");
        setIsLoading(true);

        const response = await fetch(
          `http://www.omdbapi.com/?apikey=${KEY}&s=${query}`,
          { signal: controller.signal }
        );

        const data = await response.json();

        if (query.length < 3) {
          setMovies([]);
          setError("");
          return;
        }

        if (data.Response === "False") {
          console.log(data.Response);
          throw new Error("No movies found containing: " + query);
        }

        setMovies(data.Search);
        setError("");
      } catch (err) {
        if (err.name !== "AbortError") {
          setError(err.message);
        }
      } finally {
        setIsLoading(false);
      }
    }, 500);

    //handleCloseMovie();
    //cleanup function:
    return function () {
      controller.abort();
      clearTimeout(waitFetch);
    };
  }, [query /*, callback */]);
  return { movies, isLoading, error };
}

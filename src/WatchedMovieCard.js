export function WatchedMovieCard({ movie, onDeleteWatched }) {
  function deleteFromWatchedList() {
    onDeleteWatched(movie.imdbID);
  }
  return (
    <li>
      <img src={movie.poster} alt={`${movie.title} poster`} />
      <h3>{movie.title}</h3>
      <div>
        <p>
          <span>⭐️</span>
          <span>{movie.imdbRating}</span>
        </p>
        <p>
          <span>🌟</span>
          <span>{movie.userRating}</span>
        </p>
        <p>
          <span>⏳</span>
          <span>{movie.runtime} min</span>
        </p>
        <p
          className="tooltip btn-delete"
          title="Delete"
          onClick={deleteFromWatchedList}
        >
          ⛔
        </p>
      </div>
    </li>
  );
}

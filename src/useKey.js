import { useEffect } from "react";

export function useKey(key, action) {
  // ESC key closes movie detail view.
  // Each time effect is executed, one more eventlistener is added,
  // so cleanup is needed (otherwise might produce memory problems in large apps)
  useEffect(() => {
    function callback(e) {
      if (e.code.toLowerCase() === key.toLowerCase()) {
        action();
      }
    }
    document.addEventListener("keydown", callback);

    return function () {
      document.removeEventListener("keydown", callback);
    };
  }, [action, key]);
}

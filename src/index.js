import React, { useState } from "react";
import ReactDOM from "react-dom/client";
// import StarRating from "./StarRating";
import "./index.css";
import App from "./App";

// function Test() {
//   const [movieRating, setMovieRating] = useState(0);
//   return (
//     <div>
//       <StarRating color="red" maxRating={5} onSetMovieRating={setMovieRating} />
//       <p>This movie was reated {movieRating} stars</p>
//     </div>
//   );
// }

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
    {/* <StarRating maxRating={5} size="45" />
    <StarRating maxRating={5} color="fuchsia" />
    <StarRating
      maxRating={5}
      color="blue"
      size="60"
      messages={["terrible", "bad", "so-so", "good", "excellent"]}
      defaultRating={5}
    />
    <StarRating maxRating={5} color="green" size="70" className="test" />
    <StarRating maxRating={5} color="maroon" size="80" />
    <Test /> */}
  </React.StrictMode>
);

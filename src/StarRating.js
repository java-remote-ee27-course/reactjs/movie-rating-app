import { useState } from "react";
import PropTypes from "prop-types";

const containerStyle = {
  display: "flex",
  alignItems: "center",
  gap: "15px",
};

const starContainerStyle = {
  display: "flex",
  //   position: "absolute",
  //   marginTop: "50px",
};

function FullStarHtml({ color }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 20 20"
      fill={color}
      stroke={color}
    >
      <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
    </svg>
  );
}

function EmptyStarHtml({ color }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      stroke={color}
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="{2}"
        d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"
      />
    </svg>
  );
}

//Specifying the type of prop in JSX
//isRequired - making it required
StarRating.propTypes = {
  maxRating: PropTypes.number,
  defaultRating: PropTypes.number,
  size: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
  messages: PropTypes.array,
  onSetRating: PropTypes.func,
};

function StarRating({
  maxRating = 1,
  color = "#FFBF00",
  size = "50",
  className = "",
  messages = [],
  defaultRating = 0,
  onSetMovieRating,
}) {
  const [currentRating, setCurrentRating] = useState(defaultRating);
  const [hoverRating, setHoverRating] = useState(0);

  const pointsContainerStyle = {
    lineHeight: "1",
    margin: "0",
    color,
    fontSize: `${size / 2}px`,
  };

  function handleCurrentRating(rating) {
    setCurrentRating(rating);
    onSetMovieRating && onSetMovieRating(rating);
  }
  function handleHoverRating(rating) {
    setHoverRating(rating);
  }

  return (
    <div style={containerStyle} className={className}>
      <div style={starContainerStyle}>
        {Array.from({ length: maxRating }, (_, i) => (
          <Star
            key={i}
            onSetRating={() => handleCurrentRating(i + 1)}
            // fullStar if hoverRating and if hoverRating is >=i+1, else if currentRating...
            fullStar={
              hoverRating ? hoverRating >= i + 1 : currentRating >= i + 1
            }
            onEnter={() => handleHoverRating(i + 1)}
            onLeave={() => handleHoverRating(0)}
            color={color}
            size={size}
          />
        ))}
      </div>
      <p style={pointsContainerStyle}>
        {messages.length === maxRating
          ? messages[hoverRating - 1] || messages[currentRating - 1] || ""
          : hoverRating || currentRating || ""}
      </p>
    </div>
  );
}

function Star({ onSetRating, fullStar, onEnter, onLeave, color, size }) {
  const starStyle = {
    width: `${size}px`,
    height: `${size}px`,
    display: "block",
    cursor: "pointer",
  };
  return (
    <span
      role="button"
      style={starStyle}
      onClick={onSetRating}
      onMouseEnter={onEnter}
      onMouseLeave={onLeave}
    >
      {fullStar ? (
        <FullStarHtml color={color} />
      ) : (
        <EmptyStarHtml color={color} />
      )}
    </span>
  );
}

export default StarRating;

import { WatchedMovieCard } from "./WatchedMovieCard";

export function WatchedMovieList({ watched, onDeleteWatched }) {
  return (
    <ul className="list">
      {watched?.map((movie) => (
        <WatchedMovieCard
          movie={movie}
          key={movie.imdbID}
          onDeleteWatched={onDeleteWatched}
        />
      ))}
    </ul>
  );
}
